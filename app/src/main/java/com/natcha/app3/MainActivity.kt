package com.natcha.app3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.natcha.app3.adapter.ItemAdapter
import com.natcha.app3.data.Datasource

class MainActivity : AppCompatActivity() {
    private val myDataset = Datasource().loadOils()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.adapter = ItemAdapter({position -> onListItemClick(position)}, this, myDataset)

        recyclerView.setHasFixedSize(true)
    }
    private fun onListItemClick(position: Int) {
        Toast.makeText(baseContext, myDataset[position].name + " ราคา: " + myDataset[position].price, Toast.LENGTH_SHORT).show()
        Log.d("MainActivity", myDataset[position].name)
    }

}
