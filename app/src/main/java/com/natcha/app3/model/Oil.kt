package com.natcha.app3.model

data class Oil(val name: String, val price: Double)
